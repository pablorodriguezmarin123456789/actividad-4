using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbrirPuerta : MonoBehaviour
{
    public Animator AnimacionPuerta;
    private bool enZona;
    private bool Activ;
    void Update()
    {
        Activ = !Activ;
        if(Input.GetKeyDown(KeyCode.E) && enZona == true)
        {
            if(Activ == true)
            {
                AnimacionPuerta.SetBool("Abrir", true);
            }
            if(Activ == false)
            {
                AnimacionPuerta.SetBool("Abrir", false);
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
         if (other.tag == "Player")
         {
            enZona = true;
         }
    }
    private void OnTriggerExit(Collider other)
    {
         if (other.tag == "Player")
         {
            enZona = false;
         }
    }
}
